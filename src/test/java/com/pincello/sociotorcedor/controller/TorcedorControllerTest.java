package com.pincello.sociotorcedor.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.pincello.sociotorcedor.SistemaSocioTorcedorApplicationTests;
import com.pincello.sociotorcedor.domain.Torcedor;
import com.pincello.sociotorcedor.helper.JsonHelper;
import com.pincello.sociotorcedor.helper.TorcedorFake;

/**
 * Testes do controller responsável pela exposição das APIs de Torcedor
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class TorcedorControllerTest extends SistemaSocioTorcedorApplicationTests {
	
	private MockMvc mockMvc;
	
	@Autowired
	TorcedorController torcedorController;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(torcedorController).build();
	}
	
	@After
	public void tearDown() {
	  JdbcTestUtils.deleteFromTables(jdbcTemplate, "torcedor");
	}
	
	@Test
	public void testPostTorcedorBadRequestEmailInvalido() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setEmail(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/torcedor")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(torcedor))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPostTorcedorBadRequestNomeInvalido() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setNomeCompleto(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/torcedor")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(torcedor))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPostTorcedorBadRequestDataNascimentoInvalida() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setDataNascimento(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/torcedor")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(torcedor))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPostTorcedorBadRequestTimeInvalido() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setTimeCoracao(null);
		this.mockMvc.perform(MockMvcRequestBuilders.post("/torcedor")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(torcedor))
				).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void testPOSTTorcedor() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.post("/torcedor")
				.contentType(MediaType.APPLICATION_JSON)
				.content(JsonHelper.toJson(TorcedorFake.create()))
				).andExpect(MockMvcResultMatchers.status().isCreated());
	}

}
