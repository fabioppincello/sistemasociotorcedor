package com.pincello.sociotorcedor.helper;

import com.github.javafaker.Faker;
import com.pincello.sociotorcedor.domain.Torcedor;
import com.pincello.sociotorcedor.domain.Time;

/**
 * Helper para geração de objetos de torcedores aleatórios
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class TorcedorFake {
	
	public static Torcedor create() {
		return create(new Faker().random().nextInt(1, 12));
	}
	
	private static Torcedor create(Integer idTimeCoracao) {
		Faker faker = new Faker();
		Torcedor torcedor = new Torcedor();
		torcedor.setEmail(faker.internet().emailAddress());
		torcedor.setNomeCompleto(faker.name().fullName());
		torcedor.setDataNascimento(faker.date().birthday());
		Time timeCoracao = new Time();
		timeCoracao.setId(idTimeCoracao);
		timeCoracao.setNome(faker.esports().team());
		torcedor.setTimeCoracao(timeCoracao);
		return torcedor;
	}

}
