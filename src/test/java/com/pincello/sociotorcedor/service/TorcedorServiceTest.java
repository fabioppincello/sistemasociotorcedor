package com.pincello.sociotorcedor.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import com.pincello.sociotorcedor.SistemaSocioTorcedorApplicationTests;
import com.pincello.sociotorcedor.domain.CadastroDTO;
import com.pincello.sociotorcedor.domain.Mensagem;
import com.pincello.sociotorcedor.domain.Torcedor;
import com.pincello.sociotorcedor.enums.MensagemEnum;
import com.pincello.sociotorcedor.exception.TorcedorInvalidoException;
import com.pincello.sociotorcedor.helper.TorcedorFake;
import com.pincello.sociotorcedor.integration.CampanhasClient;
import com.pincello.sociotorcedor.persistence.TorcedoresRepository;

/**
 * Testes da classe que implementa as regras de negócio referente aos torcedores
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class TorcedorServiceTest extends SistemaSocioTorcedorApplicationTests {
	
	@Autowired
	private TorcedoresRepository torcedoresRepository;
	
	@Autowired
	private TorcedorService torcedorService;
	
	private CampanhasClient campanhasClient;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Before
	public void SetUp() {
		campanhasClient = mock(CampanhasClient.class);
		torcedorService.setCampanhasClient(campanhasClient);
	}
	
	@After
	public void tearDown() {
	  JdbcTestUtils.deleteFromTables(jdbcTemplate, "torcedor");
	}
	
	@Test(expected = TorcedorInvalidoException.class)
	public void testCadastrarTorcedorEmailInvalido() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setEmail(null);
		torcedorService.cadastrarTorcedor(torcedor);
	}
	
	@Test(expected = TorcedorInvalidoException.class)
	public void testCadastrarTorcedorNomeInvalido() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setNomeCompleto(null);
		torcedorService.cadastrarTorcedor(torcedor);
	}
	
	@Test(expected = TorcedorInvalidoException.class)
	public void testCadastrarTorcedorDataInvalida() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setDataNascimento(null);
		torcedorService.cadastrarTorcedor(torcedor);
	}
	
	@Test(expected = TorcedorInvalidoException.class)
	public void testCadastrarTorcedorTimeInvalido() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		torcedor.setTimeCoracao(null);
		torcedorService.cadastrarTorcedor(torcedor);
	}
	
	@Test
	public void testCadastrarTorcedor() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		CadastroDTO cadastro = torcedorService.cadastrarTorcedor(torcedor);
		assertNotNull(cadastro.getTorcedor());
		assertTrue(torcedoresRepository.existsById(cadastro.getTorcedor().getEmail()));
	}
	
	@Test
	public void testCadastrarTorcedorExistente() throws Exception {
		Torcedor torcedor = TorcedorFake.create();
		CadastroDTO cadastro = torcedorService.cadastrarTorcedor(torcedor);
		assertNotNull(cadastro.getTorcedor());
		assertTrue(torcedoresRepository.existsById(cadastro.getTorcedor().getEmail()));
		cadastro = torcedorService.cadastrarTorcedor(cadastro.getTorcedor());
		assertTrue(cadastro.getMensagens().contains(new Mensagem(MensagemEnum.MSG_TORCEDOR_EXISTENTE)));
	}

}
