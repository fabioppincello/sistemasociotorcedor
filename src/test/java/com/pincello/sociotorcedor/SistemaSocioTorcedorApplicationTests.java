package com.pincello.sociotorcedor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SistemaSocioTorcedorApplicationTests {

	@Test
	public void contextLoads() {
	}

}
