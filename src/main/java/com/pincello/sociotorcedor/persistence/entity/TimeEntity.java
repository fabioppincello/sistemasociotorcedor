package com.pincello.sociotorcedor.persistence.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pincello.sociotorcedor.domain.Time;

/**
 * Entidade para a tabela de times
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@Entity
@Table(name = "time")
public class TimeEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5418340732364821666L;
	
	@Id
	@GeneratedValue
	private Integer id;
	private String nome;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}	
	
	public Time convertToDomain() {
		Time time = new Time();
		time.setId(id);
		time.setNome(nome);
		return time;
	}

}
