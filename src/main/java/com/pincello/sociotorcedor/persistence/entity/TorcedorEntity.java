package com.pincello.sociotorcedor.persistence.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.pincello.sociotorcedor.domain.Campanha;
import com.pincello.sociotorcedor.domain.Torcedor;

/**
 * Entidade para a tabela de torcedores
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@Entity
@Table(name = "torcedor")
public class TorcedorEntity {
	
	@Id
	private String email;
	private String nomeCompleto;
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	private TimeEntity timeCoracao;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<CampanhaEntity> campanhas;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNomeCompleto() {
		return nomeCompleto;
	}
	
	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}
	
	public Date getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public TimeEntity getTimeCoracao() {
		return timeCoracao;
	}
	
	public void setTimeCoracao(TimeEntity timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

	public List<CampanhaEntity> getCampanhas() {
		return campanhas;
	}

	public void setCampanhas(List<CampanhaEntity> campanhas) {
		this.campanhas = campanhas;
	}
	
	public Torcedor convertToDomain() {
		Torcedor torcedor = new Torcedor();
		torcedor.setEmail(email);
		torcedor.setNomeCompleto(nomeCompleto);
		torcedor.setDataNascimento(dataNascimento);
		torcedor.setTimeCoracao(timeCoracao.convertToDomain());
		List<Campanha> campanhas = new ArrayList<Campanha>();
		if(this.campanhas != null && !this.campanhas.isEmpty()) {
			for(CampanhaEntity campanha : this.campanhas) {
				campanhas.add(campanha.convertToDomain());
			}
		}
		torcedor.setCampanhas(campanhas);
		return torcedor;
	}

}
