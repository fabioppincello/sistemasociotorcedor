package com.pincello.sociotorcedor.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pincello.sociotorcedor.persistence.entity.TorcedorEntity;

/**
 * Repository de torcedores
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public interface TorcedoresRepository extends JpaRepository<TorcedorEntity, String> {

}
