package com.pincello.sociotorcedor.service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pincello.sociotorcedor.domain.CadastroDTO;
import com.pincello.sociotorcedor.domain.Campanha;
import com.pincello.sociotorcedor.domain.Mensagem;
import com.pincello.sociotorcedor.domain.Torcedor;
import com.pincello.sociotorcedor.enums.MensagemEnum;
import com.pincello.sociotorcedor.exception.TorcedorInvalidoException;
import com.pincello.sociotorcedor.integration.CampanhasClient;
import com.pincello.sociotorcedor.integration.exception.ServicoCampanhasIndisponivelException;
import com.pincello.sociotorcedor.persistence.TorcedoresRepository;
import com.pincello.sociotorcedor.persistence.entity.TorcedorEntity;

/**
 * Classe que implementa as regras de negócio referente aos torcedores
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@Service
public class TorcedorService {
	
	private static final Logger logger = LogManager.getLogger(TorcedorService.class);
	
	@Autowired
	private TorcedoresRepository torcedoresRepository;
	
	@Autowired
	private CampanhasClient campanhasClient;
	
	public void setCampanhasClient(CampanhasClient campanhasClient) {
		this.campanhasClient = campanhasClient;
	}
	
	/**
	 * Método responsável por manipular o cadastro de um torcedor.
	 * 
	 * @param torcedor
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public CadastroDTO cadastrarTorcedor(Torcedor torcedor) throws Exception {
		logger.trace(String.format("cadastrarTorcedor(%s)", torcedor));
		
		validarTorcedor(torcedor);
		
		CadastroDTO cadastro = torcedoresRepository.existsById(torcedor.getEmail()) ?
				tratarTorcedorCadastrado(torcedor) :
				cadastrarNovoTorcedor(torcedor);
		
		return cadastro;
	}
	
	/**
	 * Método responsável pela validação dos campos de um objeto de torcedor.
	 * 
	 * @param torcedor
	 * @throws TorcedorInvalidoException
	 */
	private void validarTorcedor(Torcedor torcedor) throws TorcedorInvalidoException {
		if(torcedor.getEmail() == null || torcedor.getEmail().isEmpty()) {
			throw new TorcedorInvalidoException(MensagemEnum.MSG_TORCEDOR_EMAIL_INVALIDO.getMensagem());
		}
		
		if(torcedor.getNomeCompleto() == null || torcedor.getNomeCompleto().isEmpty()) {
			throw new TorcedorInvalidoException(MensagemEnum.MSG_TORCEDOR_NOME_INVALIDO.getMensagem());
		}
		
		if(torcedor.getDataNascimento() == null) {
			throw new TorcedorInvalidoException(MensagemEnum.MSG_TORCEDOR_DATA_INVALIDA.getMensagem());
		}
		
		if(torcedor.getTimeCoracao() == null || torcedor.getTimeCoracao().getId() == null) {
			throw new TorcedorInvalidoException(MensagemEnum.MSG_TORCEDOR_TIME_INVALIDO.getMensagem());
		}
	}
	
	/**
	 * Método responsável por cadastrar um novo torcedor.
	 * As campanhas relacionadas ao seu time do coração são adicionadas
	 * na lista de campanhas.
	 * 
	 * @param torcedor
	 * @return
	 */
	private CadastroDTO cadastrarNovoTorcedor(Torcedor torcedor) {
		CadastroDTO cadastro = new CadastroDTO();
		
		torcedor = create(torcedor);
		try {
			associarCampanhas(torcedor);
		} catch (ServicoCampanhasIndisponivelException e) {
			cadastro.getMensagens().add(new Mensagem(MensagemEnum.MSG_ERRO_ASSOCIAR_CAMPANHAS, e.getMessage()));
		}
		cadastro.setTorcedor(torcedor);
		
		return cadastro;
	}
	
	/**
	 * Método responsável por criar um novo torcedor na base de dados.
	 * 
	 * @param torcedor
	 * @return
	 */
	private Torcedor create(Torcedor torcedor) {
		TorcedorEntity torcedorEntity = torcedor.convertToEntity();
		torcedorEntity = torcedoresRepository.save(torcedorEntity);
		
		torcedor = torcedorEntity.convertToDomain();
		
		return torcedor;
	}
	
	/**
	 * Método responsável por manipular o cadastro de um torcedor já existente.
	 * Dependendo das campanhas cadastradas no sistema de campanhas, as
	 * campanhas do torcedor podem ser atualizadas ou excluídas.
	 * Além disso, as novas campanhas relacionadas ao seu time do coração
	 * são adicionadas na lista de campanhas.
	 * 
	 * @param torcedor
	 * @return
	 */
	private CadastroDTO tratarTorcedorCadastrado(Torcedor torcedor) {
		CadastroDTO cadastro = new CadastroDTO();
		cadastro.getMensagens().add(new Mensagem(MensagemEnum.MSG_TORCEDOR_EXISTENTE, torcedor.getEmail()));
		Optional<TorcedorEntity> torcedorEntity = torcedoresRepository.findById(torcedor.getEmail());
		if(torcedorEntity != null) {
			torcedor = torcedorEntity.get().convertToDomain();
			boolean enviarNovasCampanhas = torcedor.getCampanhas() == null || torcedor.getCampanhas().isEmpty();
			
			try {
				associarCampanhas(torcedor);
				if(enviarNovasCampanhas) {
					cadastro.setNovasCampanhas(torcedor.getCampanhas());
				}
			} catch (ServicoCampanhasIndisponivelException e) {
				cadastro.getMensagens().add(new Mensagem(MensagemEnum.MSG_ERRO_ASSOCIAR_NOVAS_CAMPANHAS, e.getMessage()));
			}
			
			cadastro.setTorcedor(torcedor);
		}
		
		return cadastro;
	}
	
	/**
	 * Método responsável pela associação de campanhas a um torcedor.
	 * Dependendo das campanhas cadastradas no sistema de campanhas, as
	 * campanhas do torcedor podem ser atualizadas ou excluídas.
	 * Além disso, as novas campanhas relacionadas ao seu time do coração
	 * são adicionadas na lista de campanhas.
	 * 
	 * @param torcedor
	 * @throws ServicoCampanhasIndisponivelException
	 */
	private void associarCampanhas(Torcedor torcedor) throws ServicoCampanhasIndisponivelException {
		boolean updated;
		if(torcedor.getCampanhas() == null || torcedor.getCampanhas().isEmpty()) {
			updated = associarTodasAsCampanhas(torcedor);
		} else {
			updated = associarNovasCampanhas(torcedor);
		}
		if(updated) {
			TorcedorEntity torcedorEntity = torcedor.convertToEntity();
			torcedorEntity = torcedoresRepository.save(torcedorEntity);
		}
	}
	
	/**
	 * Método responsável por associar campanhas a um torcedor,
	 * baseado no seu time do coração.
	 * 
	 * @param torcedor
	 * @return
	 * @throws ServicoCampanhasIndisponivelException
	 */
	private boolean associarTodasAsCampanhas(Torcedor torcedor) throws ServicoCampanhasIndisponivelException {
		List<Campanha> campanhasTime = campanhasClient.getCampanhas(torcedor.getTimeCoracao().getId());
		if(campanhasTime != null && !campanhasTime.isEmpty()) {
			torcedor.setCampanhas(campanhasTime);
			return true;
		}
		return false;
	}
	
	/**
	 * Método responsável por atualizar a lista de campanhas de um torcedor
	 * com base nas campanhas cadastradas no sistema de campanhas para o seu
	 * time do coração.
	 * As campanhas que não estiverem mais cadastradas para seu time do coração
	 * são excluídas da sua lista de campanhas.
	 * As campanhas que tiverem com versões diferentes serão atualizadas na sua
	 * lista de campanhas.
	 * Novas campanhas do seu time do coração serão adicionadas à sua lista de campanhas.
	 * 
	 * @param torcedor
	 * @return
	 * @throws ServicoCampanhasIndisponivelException
	 */
	private boolean associarNovasCampanhas(Torcedor torcedor) throws ServicoCampanhasIndisponivelException {
		List<Campanha> campanhasTime = campanhasClient.getCampanhas(torcedor.getTimeCoracao().getId());
		if(campanhasTime != null && !campanhasTime.isEmpty()) {
			boolean updated = desassociarCampanhasDeletadas(torcedor, campanhasTime);
			for(Campanha campanhaTime : campanhasTime) {
				boolean addCampanha = true;
				for(Campanha campanha : torcedor.getCampanhas()) {
					if(campanhaTime.getId().equals(campanha.getId())) {
						if(!campanhaTime.getVersao().equals(campanha.getVersao())) {
							torcedor.getCampanhas().set(torcedor.getCampanhas().indexOf(campanha), campanhaTime);
							updated = true;
						}
						addCampanha = false;
						break;
					}
				}
				if(addCampanha) {
					torcedor.getCampanhas().add(campanhaTime);
					updated = true;
				}
			}
			return updated;
		}
		return false;
	}
	
	/**
	 * Método responsável por remover da lista de campanhas de um torcedor
	 * aquelas que não estiverem mais cadastradas para seu time do coração
	 * no sistema de campanhas.
	 * 
	 * @param torcedor
	 * @param campanhasTime
	 * @return
	 */
	private boolean desassociarCampanhasDeletadas(Torcedor torcedor, List<Campanha> campanhasTime) {
		boolean updated = false;
		Iterator<Campanha> iterator = torcedor.getCampanhas().iterator();
		while(iterator.hasNext()) {
			Campanha campanha = iterator.next();
			boolean deletada = true;
			for(Campanha campanhaTime : campanhasTime) {
				if(campanha.getId().equals(campanhaTime.getId())) {
					deletada = false;
					break;
				}
			}
			if(deletada) {
				iterator.remove();
				updated = true;
			}
		}
		return updated;
	}

}
