package com.pincello.sociotorcedor.exception;

/**
 * Exceção utilizada para informar que um objeto de torcedor está em um formato inválido
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class TorcedorInvalidoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4836682102372553414L;
	
	public TorcedorInvalidoException(String message) {
		super(message);
	}

}
