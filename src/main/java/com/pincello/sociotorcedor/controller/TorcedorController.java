package com.pincello.sociotorcedor.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pincello.sociotorcedor.domain.CadastroDTO;
import com.pincello.sociotorcedor.domain.Mensagem;
import com.pincello.sociotorcedor.domain.Response;
import com.pincello.sociotorcedor.domain.Torcedor;
import com.pincello.sociotorcedor.enums.MensagemEnum;
import com.pincello.sociotorcedor.exception.TorcedorInvalidoException;
import com.pincello.sociotorcedor.service.TorcedorService;

/**
 * 
 * Controller responsável pela exposição das APIs de Torcedor
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@RestController
@RequestMapping("/torcedor")
public class TorcedorController {
	
	private static final Logger logger = LogManager.getLogger(TorcedorController.class);
	
	@Autowired
	private TorcedorService torcedorService;
	
	/**
	 * Método utilizado para expor o cadastro de um torcedor
	 * 
	 * @param torcedor
	 * @param result
	 * @return
	 */
	@PostMapping
	public ResponseEntity<Response<CadastroDTO>> createTorcedor(@Valid @RequestBody Torcedor torcedor, BindingResult result) {
		logger.trace(String.format("createTorcedor(%s)", torcedor));
		
		Response<CadastroDTO> response = new Response<CadastroDTO>();
		response.setBindingResultErrors(result);
		
		if(result.hasErrors()) {
			return new ResponseEntity<Response<CadastroDTO>>(response, HttpStatus.BAD_REQUEST);
		}
		
		CadastroDTO cadastroResponse = null;
		try {
			cadastroResponse = torcedorService.cadastrarTorcedor(torcedor);
		} catch(TorcedorInvalidoException e) {
			response.getErrors().add(e.getMessage());
			return new ResponseEntity<Response<CadastroDTO>>(response, HttpStatus.BAD_REQUEST);
		} catch(Exception e) {
			logger.error(e);
			if(e.getMessage() != null) {
				response.getErrors().add(e.getMessage());
			} else {
				response.getErrors().add(e.toString());
			}
			return new ResponseEntity<Response<CadastroDTO>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.setData(cadastroResponse);
		
		if(cadastroResponse.getMensagens().contains(new Mensagem(MensagemEnum.MSG_TORCEDOR_EXISTENTE))) {
			return new ResponseEntity<Response<CadastroDTO>>(response, HttpStatus.OK);
		}
		
		return new ResponseEntity<Response<CadastroDTO>>(response, HttpStatus.CREATED);
	}

}
