package com.pincello.sociotorcedor.enums;

/**
 * Enum utilizado para fornecer as mensagens utilizadas pelo sistema.
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public enum MensagemEnum {
	
	MSG_TORCEDOR_EXISTENTE(1, "O cadastro do torcedor %s já foi efetuado."),
	MSG_ERRO_ASSOCIAR_CAMPANHAS(2, "Não foi possível associar as campanhas, tente mais tarde. Motivo: %s"),
	MSG_ERRO_ASSOCIAR_NOVAS_CAMPANHAS(3, "Não foi possível associar novas campanhas, tente mais tarde. Motivo: %s"),
	MSG_TORCEDOR_ENCONTRADO(4, "Torcedor [%s] não encontrado."),
	MSG_TORCEDOR_EMAIL_INVALIDO(5, "O email do torcedor não foi inserido."),
	MSG_TORCEDOR_NOME_INVALIDO(6, "O nome do torcedor não foi inserido."),
	MSG_TORCEDOR_DATA_INVALIDA(7, "A data de nascimento do torcedor não foi inserida."),
	MSG_TORCEDOR_TIME_INVALIDO(8, "O time do torcedor não foi inserida.");
	
	private int codigo;
	private String format;
	
	MensagemEnum(int codigo, String format) {
		this.codigo = codigo;
		this.format = format;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public String getMensagem(Object... args) {
		return String.format(format, args);
	}
	
	public String getMensagem() {
		return format;
	}

}
