package com.pincello.sociotorcedor.integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pincello.sociotorcedor.domain.Campanha;
import com.pincello.sociotorcedor.domain.Response;
import com.pincello.sociotorcedor.integration.exception.ServicoCampanhasIndisponivelException;

/**
 * Classe responsável por chamar APIs de campanhas
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@Service
@PropertySource("classpath:integration.properties")
public class CampanhasClient {
	
	@Value("${url-obter-campanhas}")
	private String URL_API;
	
	private static final Logger logger = LogManager.getLogger(CampanhasClient.class);
	
	private final RestTemplate restTemplate;
	
	public CampanhasClient(RestTemplateBuilder restTemplateBuilder) {
		restTemplate = restTemplateBuilder.build();
	}
	
	/**
	 * Método responsável por chamar a API de busca de campanhas por time.
	 * A busca é realizada pelo id do time.
	 * 
	 * @param idTime
	 * @return
	 * @throws ServicoCampanhasIndisponivelException
	 */
	public List<Campanha> getCampanhas(Integer idTime) throws ServicoCampanhasIndisponivelException {
		logger.info(String.format("getCampanhas(%s)", idTime));
		ResponseEntity<String> responseEntity = null;
		try {
			responseEntity = restTemplate.getForEntity(String.format(URL_API, idTime), String.class);
		} catch(Exception e) {
			logger.error(e);
			throw new ServicoCampanhasIndisponivelException();
		}
		
		if(responseEntity.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
			return new ArrayList<Campanha>();
		}
		
		ObjectMapper mapper = new ObjectMapper();
		Response<List<Campanha>> response;
		try {
			response = mapper.readValue(responseEntity.getBody(), new TypeReference<Response<List<Campanha>>>() {});
		} catch (IOException e) {
			logger.error(e);
			throw new ServicoCampanhasIndisponivelException();
		}
		
		return response.getData();
	}

}
