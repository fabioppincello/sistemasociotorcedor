package com.pincello.sociotorcedor.integration.exception;

/**
 * Exceção utilizada para informar que a API de campanhas não está disponível
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class ServicoCampanhasIndisponivelException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 914529295753243531L;
	
	public ServicoCampanhasIndisponivelException() {
		super("O serviço de campanhas está indisponível.");
	}

}
