package com.pincello.sociotorcedor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaSocioTorcedorApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SistemaSocioTorcedorApplication.class, args);
	}

}
