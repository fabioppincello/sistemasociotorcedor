package com.pincello.sociotorcedor.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.pincello.sociotorcedor.persistence.entity.CampanhaEntity;
import com.pincello.sociotorcedor.persistence.entity.TorcedorEntity;

/**
 * Classe utilizada para modelagem de um torcedor
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class Torcedor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1107436990038388037L;

	@NotNull @NotBlank @Email
	private String email;
	
	@NotNull @NotBlank
	private String nomeCompleto;
	
	@NotNull @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy z")
	private Date dataNascimento;
	
	@NotNull
	private Time timeCoracao;
	
	private List<Campanha> campanhas;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Time getTimeCoracao() {
		return timeCoracao;
	}

	public void setTimeCoracao(Time timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

	public List<Campanha> getCampanhas() {
		if(campanhas == null) {
			campanhas = new ArrayList<Campanha>();
		}
		return campanhas;
	}

	public void setCampanhas(List<Campanha> campanhas) {
		this.campanhas = campanhas;
	}
	
	public TorcedorEntity convertToEntity() {
		TorcedorEntity entity = new TorcedorEntity();
		entity.setEmail(email);
		entity.setNomeCompleto(nomeCompleto);
		entity.setDataNascimento(dataNascimento);
		entity.setTimeCoracao(timeCoracao.convertToEntity());
		List<CampanhaEntity> campanhas = new ArrayList<CampanhaEntity>();
		if(this.campanhas != null && !this.campanhas.isEmpty()) {
			for(Campanha campanha : this.campanhas) {
				campanhas.add(campanha.convertToEntity());
			}
		}
		entity.setCampanhas(campanhas);
		return entity;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder
			.append("{ email: \"")
			.append(email)
			.append("\", nomeCompleto: \"")
			.append(nomeCompleto)
			.append("\", dataNascimento: \"")
			.append(dataNascimento)
			.append("\", timeCoracao: ")
			.append(timeCoracao)
			.append(" }");
		
		return builder.toString();
	}

}
