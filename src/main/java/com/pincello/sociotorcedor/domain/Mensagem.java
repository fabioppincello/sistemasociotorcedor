package com.pincello.sociotorcedor.domain;

import java.io.Serializable;

import com.pincello.sociotorcedor.enums.MensagemEnum;

/**
 * Classe utilizada para modelagem de uma mensagem de retorno
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class Mensagem implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -724584555805165724L;
	
	private int codigo;
	private String descricao;
	
	public Mensagem(MensagemEnum mensagemEnum) {
		this.codigo = mensagemEnum.getCodigo();
		this.descricao = mensagemEnum.getMensagem();
	}
	
	public Mensagem(MensagemEnum mensagemEnum, Object... args) {
		this.codigo = mensagemEnum.getCodigo();
		this.descricao = mensagemEnum.getMensagem(args);
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensagem other = (Mensagem) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}

}
