package com.pincello.sociotorcedor.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Classe utilizada para modelagem de um cadastro de Torcedor
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
@JsonInclude(Include.NON_NULL)
public class CadastroDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2896872752984780860L;
	
	private Torcedor torcedor;
	private List<Campanha> novasCampanhas;
	private List<Mensagem> mensagens;
	
	public Torcedor getTorcedor() {
		return torcedor;
	}
	
	public void setTorcedor(Torcedor torcedor) {
		this.torcedor = torcedor;
	}
	
	public List<Campanha> getNovasCampanhas() {
		return novasCampanhas;
	}
	
	public void setNovasCampanhas(List<Campanha> novasCampanhas) {
		this.novasCampanhas = novasCampanhas;
	}
	
	public List<Mensagem> getMensagens() {
		if(mensagens == null) {
			mensagens = new ArrayList<Mensagem>();
		}
		return mensagens;
	}

	public void setMensagens(List<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}

}
