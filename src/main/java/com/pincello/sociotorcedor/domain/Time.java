package com.pincello.sociotorcedor.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.pincello.sociotorcedor.persistence.entity.TimeEntity;

/**
 * Classe utilizada para modelagem de um time
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class Time implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1314522531986309849L;
	
	@NotNull
	private Integer id;
	private String nome;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}	
	
	public TimeEntity convertToEntity() {
		TimeEntity entity = new TimeEntity();
		entity.setId(id);
		entity.setNome(nome);
		return entity;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder
			.append("{ id: ")
			.append(id)
			.append(", nome: \"")
			.append(nome)
			.append("\" }");
		
		return builder.toString();
	}

}
